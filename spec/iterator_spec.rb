require 'spec_helper'

RSpec.describe Iterator do
  context 'Iterator::iterate_platforms' do
    it 'doesnt iterate through an empty list' do
      expect(Iterator).to_not receive(:iterate_platform_versions)

      Iterator.iterate_platforms([])
    end

    it 'iterates through a singleton list' do
      expect(Iterator).to receive(:iterate_platform_versions).with('test').once

      Iterator.iterate_platforms(['test'])
    end

    it 'iterates through a multi-value list' do
      expect(Iterator).to receive(:iterate_platform_versions).with('test').once
      expect(Iterator).to receive(:iterate_platform_versions).with('abc').once

      Iterator.iterate_platforms(['test', 'abc'])
    end

    it 'iterates through a multi-value list' do
      expect(Iterator).to receive(:iterate_platform_versions).exactly(4).times

      Iterator.iterate_platforms(%w(this is an array))
    end
  end
end
