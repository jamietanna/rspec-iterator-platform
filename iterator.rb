class Iterator
  def self.iterate_platforms(platforms)
    platforms.each do |platform|
      iterate_platform_versions(platform)
    end
  end

  def self.iterate_platform_versions(platform)
    platform.each do |x|
      x.each do |y, z|
        y.each do |p|
          yield p
        end
      end
    end
  end
end
